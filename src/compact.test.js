import compact from './compact';

describe('when using compact', () => {
  const falsey = [false, null, 0, '', undefined, NaN];

  it('should filter falsey values', () => {
    const array = ['0', '1', '2'];
    expect(compact([...array, ...falsey])).toEqual(array);
  });

  it('should filter falsey values in between array', () => {
    const array = ['0', false, '1', null, 0, '2', undefined, '', NaN];
    expect(compact(array)).toEqual(['0', '1', '2']);
  });

  it('should return directly if array is null or undefined', () => {
    expect(compact(null)).toEqual(null);
    expect(compact(undefined)).toEqual(undefined);
  });

  it('should alway return new array rather than change the original one', () => {
    const array = ['0', false, '1', null, 0, '2', undefined, '', NaN];
    const newArray = compact(array);

    expect(newArray).not.toEqual(array);
  });
});
